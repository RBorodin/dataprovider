package ru.bellintegrator.dataProvider.utils;


import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONObject;
import ru.bellintegrator.dataProvider.model.Lockable;
import ru.bellintegrator.dataProvider.utils.dataLocker.DataLockerImp;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Predicate;


public class Predicates {

    private static final DataLockerImp dataLockerImp = new DataLockerImp();

    /**
     * @return
     */
    public static Predicate<Lockable> notLocked() {
        return cp -> dataLockerImp.checkAvailable(cp.getRowIDs());
    }

    /**
     * @param filter
     * @return
     */
    public static Predicate<Lockable> checkHasParams(String filter) {
        JSONObject jsonObject = new JSONObject(filter);
        String jsonString = jsonObject.toString();
        ObjectMapper oMapper = new ObjectMapper();
        oMapper.findAndRegisterModules();
        return cp -> {
            try {
                HashMap result = new ObjectMapper().readValue(jsonString, HashMap.class);
                Map map = oMapper.convertValue(cp, Map.class);
                return map.entrySet().containsAll(result.entrySet());
            } catch (IOException e) {
                e.printStackTrace();
            }
            return false;
        };
    }
}

