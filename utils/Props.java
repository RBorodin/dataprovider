package ru.bellintegrator.dataProvider.utils;

import java.io.IOException;
import java.util.Properties;

public class Props {

    /**
     * @param property
     * @return
     */
    public static String readProp(String property) {
        Properties prop = new Properties();
        try {
            prop.load(Props.class.getClassLoader().getResourceAsStream("application.properties"));
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return prop.getProperty(property);
    }

}
