package ru.bellintegrator.dataProvider.utils.dataFormatter;

import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;
import com.opencsv.bean.HeaderColumnNameMappingStrategy;
import ru.bellintegrator.dataProvider.utils.Props;

import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

public class CsvUtils {
    static String testDataCsvPath = Props.readProp("testData.csv.path");

    /**
     * Метод считывать данные из csv файла в список java-объектов.
     *
     * @param clazz    - тип класса для сохранения данных
     * @param filePath - путь к читаемому файлу
     * @return список выбранных объектов, инициализированных на основе данных из csv файла
     */
    public static <T> List<T> readCsvToBeanList(String filePath, Class<T> clazz) {

        HeaderColumnNameMappingStrategy<T> ms = new HeaderColumnNameMappingStrategy<>();
        ms.setType(clazz);
        Path path = Paths.get(testDataCsvPath + filePath);
        List<T> list = null;
        try (Reader reader = Files.newBufferedReader(path)) {
            CsvToBean<Object> cb = new CsvToBeanBuilder<>(reader)
                    .withType(clazz)
                    .withMappingStrategy(ms)
                    .build();
            list = (List<T>) cb.parse();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return list;
    }
}
