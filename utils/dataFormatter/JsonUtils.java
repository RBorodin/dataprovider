package ru.bellintegrator.dataProvider.utils.dataFormatter;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import ru.bellintegrator.dataProvider.exception.InvalidQuerySourceException;
import ru.bellintegrator.dataProvider.utils.Props;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;

public class JsonUtils {

    private static final ObjectMapper mapper = new ObjectMapper();
    static String testDataJsonPath = Props.readProp("testData.json.path");

    static {
        mapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        mapper.configure(DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES, false);
        mapper.configure(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT, true);
        mapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
        mapper.registerModule(new JavaTimeModule());
    }

    /**
     * @param filePath
     * @return
     */
    public static InputStream getJsonResource(String filePath) {
        try {
            return Files.newInputStream(Path.of(testDataJsonPath + filePath));
        } catch (IOException e) {
            e.printStackTrace();
        }
        throw new InvalidQuerySourceException("ошибка чтения json-файла");
    }

    public static <T> T fromJSON(final TypeReference<T> type,
                                 final String filePath) {
        T data = null;
        try {
            data = mapper.readValue(getJsonResource(filePath), type);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return data;
    }


}

