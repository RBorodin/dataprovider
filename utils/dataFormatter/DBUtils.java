package ru.bellintegrator.dataProvider.utils.dataFormatter;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;
import ru.bellintegrator.dataProvider.exception.InvalidQuerySourceException;
import ru.bellintegrator.dataProvider.utils.Props;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;

@Component
public class DBUtils {

    static String testDataSqlPath = Props.readProp("testData.sql.path");
    @Qualifier("datasource1JdbcTemplate")
    private static NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    /**
     * @param namedParameterJdbcTemplate
     */
    public DBUtils(NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
        DBUtils.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
    }


    /**
     * Метод позволяет считывать данные из файла в строку sql-запроса
     *
     * @param path - путь к файлу
     * @return - sql запрос в строчном виде
     * @throws InvalidQuerySourceException
     */
    public static String readSqlCommandFromFile(String path) throws InvalidQuerySourceException {
        try {
            return Files.readString(Path.of(testDataSqlPath + path), StandardCharsets.US_ASCII);
        } catch (IOException e) {
            e.printStackTrace();
        }
        throw new InvalidQuerySourceException("Ошибка чтения источника sql запроса!");
    }

    /**
     * @return
     */
    public static NamedParameterJdbcTemplate getNamedParameterJdbcTemplate() {
        return namedParameterJdbcTemplate;
    }
}
