package ru.bellintegrator.dataProvider.utils.dataLocker;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Set;
import java.util.TreeSet;

@Component
public class DataLockerImp implements DataLocker {

    private static final Set<String> itemsLocked = new TreeSet<>();
    Logger LOGGER = LoggerFactory.getLogger(DataLockerImp.class);

    @Override
    public void lock(List<String> table_id) {
        itemsLocked.addAll(table_id);
        LOGGER.info("локируются записи: {}", table_id);
        countLocked();
    }

    @Override
    public void release(List<String> table_id) {
        LOGGER.info("освобождаются записи: {}", table_id);
        table_id.forEach(itemsLocked::remove);
        countLocked();
    }

    @Override
    public void releaseAll() {
        LOGGER.info("освобождаются все записи");
        itemsLocked.clear();
        countLocked();
    }

    @Override
    public Long countLocked() {
        LOGGER.info("Всего занято {} записей", itemsLocked.size());
        return (long) itemsLocked.size();
    }

    @Override
    public boolean checkAvailable(List<String> rowsForLock) {
        rowsForLock.retainAll(itemsLocked);
        return rowsForLock.size() == 0;
    }
}
