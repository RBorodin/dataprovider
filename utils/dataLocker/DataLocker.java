package ru.bellintegrator.dataProvider.utils.dataLocker;


import java.util.List;

public interface DataLocker {

    /**
     * @param table_id
     */
    void lock(List<String> table_id);

    /**
     * @param table_id
     */
    void release(List<String> table_id);

    /**
     *
     */
    void releaseAll();

    /**
     * @return
     */
    Long countLocked();

    /**
     * @param rowsForLock
     * @return
     */
    boolean checkAvailable(List<String> rowsForLock);
}
