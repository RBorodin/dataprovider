package ru.bellintegrator.dataProvider.сonfig;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import javax.sql.DataSource;

@Configuration
public class MultipleDBConfig {
    @Bean(name = "datasource1")
    @ConfigurationProperties(prefix = "spring.datasource1")
    public DataSource datasource1() {
        return DataSourceBuilder.create().build();
    }

    @Bean(name = "datasource1JdbcTemplate")
    public NamedParameterJdbcTemplate namedParameterJdbcTemplate(@Qualifier("datasource1") DataSource datasource1) {
        return new NamedParameterJdbcTemplate(datasource1);
    }


}





