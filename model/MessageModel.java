package ru.bellintegrator.dataProvider.model;

/**
 * Данный класс создан для формирования сообщений
 * в течение работы приложения
 */
public class MessageModel {
    private String message;

    public MessageModel(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
