package ru.bellintegrator.dataProvider.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.opencsv.bean.CsvBindByName;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Deposit implements Lockable {

    @CsvBindByName(column = "deposit_id")
    int deposit_id;

    @CsvBindByName(column = "acc_num")
    String acc_num;

    @CsvBindByName(column = "branch_id")
    int branch_id;

    @CsvBindByName(column = "client_id")
    int client_id;

    @CsvBindByName(column = "balance")
    BigDecimal balance;

    @CsvBindByName(column = "state")
    int state;

    @CsvBindByName(column = "type")
    int type;

    @CsvBindByName(column = "currency_code")
    String currency_code;

    public Deposit() {
    }

    @JsonProperty("deposit_id")
    public int getDeposit_id() {
        return deposit_id;
    }

    public void setDeposit_id(int deposit_id) {
        this.deposit_id = deposit_id;
    }

    @JsonProperty("acc_num")
    public String getAcc_num() {
        return acc_num;
    }

    public void setAcc_num(String acc_num) {
        this.acc_num = acc_num;
    }

    @JsonProperty("branch_id")
    public int getBranch_id() {
        return branch_id;
    }

    public void setBranch_id(int branch_id) {
        this.branch_id = branch_id;
    }

    @JsonProperty("client_id")
    public int getClient_id() {
        return client_id;
    }

    public void setClient_id(int client_id) {
        this.client_id = client_id;
    }

    @JsonProperty("balance")
    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    @JsonProperty("state")
    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    @JsonProperty("type")
    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    @JsonProperty("currency_code")
    public String getCurrency_code() {
        return currency_code;
    }

    public void setCurrency_code(String currency_code) {
        this.currency_code = currency_code;
    }

    @Override
    public List<String> getRowIDs() {
        List<String> list = new ArrayList<>();
        list.add("commons.client" + client_id);
        list.add("commons.deposit" + deposit_id);
        return list;
    }
}
