package ru.bellintegrator.dataProvider.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.CsvDate;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ClientPersonCard implements Lockable {

    @CsvBindByName(column = "client_id")
    int client_id;

    @CsvBindByName(column = "surname")
    String surname;

    @CsvBindByName(column = "firstname")
    String firstname;

    @CsvBindByName(column = "secondname")
    String secondname;

    @CsvDate(value = "yyyy.MM.dd")
    @CsvBindByName(column = "birth")
    LocalDate birth;

    @CsvBindByName(column = "state")
    int state;

    @CsvBindByName(column = "email")
    String email;

    @CsvBindByName(column = "phone")
    String phone;

    @CsvBindByName(column = "gender")
    String gender;

    @CsvBindByName(column = "address")
    String address;

    @CsvBindByName(column = "personCard_id")
    int personCard_id;

    @CsvBindByName(column = "series")
    String series;

    @CsvBindByName(column = "no")
    String no;

    @CsvBindByName(column = "type")
    int type;

    @CsvDate(value = "yyyy.MM.dd")
    @CsvBindByName(column = "expiry")
    LocalDate expiry;

    @CsvBindByName(column = "client_state")
    int client_state;


    public ClientPersonCard() {
    }

    @JsonProperty("client_id")
    public int getClient_id() {
        return client_id;
    }

    public void setClient_id(int client_id) {
        this.client_id = client_id;
    }

    @JsonProperty("address")
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @JsonProperty("personCard_id")
    public int getPersonCard_id() {
        return personCard_id;
    }

    public void setPersonCard_id(int personCard_id) {
        this.personCard_id = personCard_id;
    }

    @JsonProperty("series")
    public String getSeries() {
        return series;
    }

    public void setSeries(String series) {
        this.series = series;
    }

    @JsonProperty("no")
    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }

    @JsonProperty("type")
    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    @JsonProperty("expiry")
    public LocalDate getExpiry() {
        return expiry;
    }

    public void setExpiry(LocalDate expiry) {
        this.expiry = expiry;
    }

    @JsonProperty("client_state")
    public int getClient_state() {
        return client_state;
    }

    public void setClient_state(int client_state) {
        this.client_state = client_state;
    }

    @JsonProperty("surname")
    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    @JsonProperty("firstname")
    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    @JsonProperty("secondname")
    public String getSecondname() {
        return secondname;
    }

    public void setSecondname(String secondname) {
        this.secondname = secondname;
    }

    @JsonProperty("birth")
    public LocalDate getBirth() {
        return birth;
    }

    public void setBirth(LocalDate birth) {
        this.birth = birth;
    }

    @JsonProperty("state")
    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    @JsonProperty("email")
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @JsonProperty("phone")
    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @JsonProperty("gender")
    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    @Override
    public List<String> getRowIDs() {
        List<String> list = new ArrayList<>();
        list.add("commons.client" + client_id);
        list.add("commons.personcard" + personCard_id);
        return list;
    }

}
