package ru.bellintegrator.dataProvider.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.CsvDate;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CCard implements Lockable {

    @CsvBindByName(column = "cCard_id")
    long cCard_id;

    @CsvBindByName(column = "number")
    String number;

    @CsvBindByName(column = "deposit_id")
    long deposit_id;

    @CsvBindByName(column = "balance")
    BigDecimal balance;

    @CsvDate(value = "yyyy.MM.dd")
    @CsvBindByName(column = "expiry")
    LocalDate expiry;

    @CsvBindByName(column = "state")
    int state;

    @CsvBindByName(column = "type")
    String type;

    @CsvBindByName(column = "currency_code")
    String currency_code;

    public CCard() {
    }

    @JsonProperty("cCard_id")
    public long getCard_id() {
        return cCard_id;
    }

    public void setCard_id(long cCard_id) {
        this.cCard_id = cCard_id;
    }

    @JsonProperty("number")
    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    @JsonProperty("deposit_id")
    public long getDeposit_id() {
        return deposit_id;
    }

    public void setDeposit_id(long deposit_id) {
        this.deposit_id = deposit_id;
    }

    @JsonProperty("balance")
    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    @JsonProperty("expiry")
    public LocalDate getExpiry() {
        return expiry;
    }

    public void setExpiry(LocalDate expiry) {
        this.expiry = expiry;
    }

    @JsonProperty("state")
    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    @JsonProperty("type")
    public String getCard_type() {
        return type;
    }

    public void setCard_type(String type) {
        this.type = type;
    }

    @JsonProperty("currency_code")
    public String getCurrency_code() {
        return currency_code;
    }

    public void setCurrency_code(String currency_code) {
        this.currency_code = currency_code;
    }

    @Override
    public List<String> getRowIDs() {
        List<String> list = new ArrayList<>();
        list.add("commons.ccard" + cCard_id);
        list.add("commons.deposit" + deposit_id);
        return list;
    }
}
