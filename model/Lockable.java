package ru.bellintegrator.dataProvider.model;

import java.util.List;

/**
 * Метод позволяет получить все ID записей в БД,
 * с которыми связана сущность (тестовый объект),
 * для их последующей блокироки и разблокировки
 *
 * @return список ID в текстовом виде
 */
public interface Lockable {
    List<String> getRowIDs();
}
