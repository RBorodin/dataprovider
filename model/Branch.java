package ru.bellintegrator.dataProvider.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.opencsv.bean.CsvBindByName;

import java.util.ArrayList;
import java.util.List;

public class Branch implements Lockable {

    @CsvBindByName(column = "id")
    int id;

    @CsvBindByName(column = "id")
    String name;

    public Branch() {
    }

    @JsonProperty("id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public List<String> getRowIDs() {
        List<String> list = new ArrayList<>();
        list.add("commons.branch" + id);
        return list;
    }
}
