package ru.bellintegrator.dataProvider.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import ru.bellintegrator.dataProvider.exception.ExistEntityException;
import ru.bellintegrator.dataProvider.exception.InvalidQuerySourceException;
import ru.bellintegrator.dataProvider.model.MessageModel;

import java.time.LocalDateTime;

@ControllerAdvice
public class ControllerExceptionHandler {

    @ExceptionHandler(ExistEntityException.class)
    public ResponseEntity<MessageModel> handleException(ExistEntityException e) {
        String message = String.format("%s %s", LocalDateTime.now(), e.getMessage());
        MessageModel response = new MessageModel(message);
        return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(InvalidQuerySourceException.class)
    public ResponseEntity<MessageModel> handleException(InvalidQuerySourceException e) {
        String message = String.format("%s %s", LocalDateTime.now(), e.getMessage());
        MessageModel response = new MessageModel(message);
        return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
    }

}