package ru.bellintegrator.dataProvider.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.bellintegrator.dataProvider.exception.ExistEntityException;
import ru.bellintegrator.dataProvider.model.Branch;
import ru.bellintegrator.dataProvider.repository.BranchRepository;
import ru.bellintegrator.dataProvider.utils.dataLocker.DataLockerImp;

import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static ru.bellintegrator.dataProvider.utils.Predicates.checkHasParams;
import static ru.bellintegrator.dataProvider.utils.Predicates.notLocked;

@RestController
@RequestMapping(value = {"/branches"})
public class BranchController {

    private final BranchRepository branchRepository;
    private final DataLockerImp dataLockerImp;

    @Autowired
    public BranchController(BranchRepository branchRepository, DataLockerImp dataLockerImp) {
        this.branchRepository = branchRepository;
        this.dataLockerImp = dataLockerImp;
    }

    @RequestMapping(value = {"/releaseByBranchId/{id}"}, method = RequestMethod.GET, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<List<String>> releaseByBranchId(@PathVariable("id") long id) {
        List<String> rowIds = branchRepository.getById(id).getRowIDs();
        dataLockerImp.release(rowIds);
        return new ResponseEntity<>(rowIds, HttpStatus.OK);
    }

    @RequestMapping(value = {"/releaseAll"}, method = RequestMethod.GET, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<Integer> releaseAll() {
        dataLockerImp.releaseAll();
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = {"/{source}/getAvailable"}, method = RequestMethod.GET, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<Branch> getAvailable(@PathVariable("source") String source) {
        Branch branch = branchRepository.getAll(source).
                stream().
                filter(notLocked()).
                findAny().
                orElseThrow(() -> new ExistEntityException("Entity not found"));
        dataLockerImp.lock(branch.getRowIDs());
        return new ResponseEntity<>(branch, HttpStatus.OK);
    }

    @RequestMapping(value = "/{source}/getAvailable", method = RequestMethod.POST, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<Branch> getAvailableByParams(@PathVariable("source") String source, @RequestBody String filter) {
        Branch branch = branchRepository.getAll(source)
                .stream()
                .filter(checkHasParams(filter))
                .filter(notLocked())
                .findAny()
                .orElseThrow(() -> new ExistEntityException("Entity not found"));
        dataLockerImp.lock(branch.getRowIDs());
        return new ResponseEntity<>(branch, HttpStatus.OK);
    }
}
