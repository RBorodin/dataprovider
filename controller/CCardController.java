package ru.bellintegrator.dataProvider.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.bellintegrator.dataProvider.exception.ExistEntityException;
import ru.bellintegrator.dataProvider.model.CCard;
import ru.bellintegrator.dataProvider.repository.CCardRepository;
import ru.bellintegrator.dataProvider.utils.dataLocker.DataLockerImp;

import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static ru.bellintegrator.dataProvider.utils.Predicates.checkHasParams;
import static ru.bellintegrator.dataProvider.utils.Predicates.notLocked;

@RestController
@RequestMapping(value = {"/cCards"})
public class CCardController {
    private final CCardRepository cCardRepository;
    private final DataLockerImp dataLockerImp;

    @Autowired
    public CCardController(CCardRepository cCardRepository, DataLockerImp dataLockerImp) {
        this.cCardRepository = cCardRepository;
        this.dataLockerImp = dataLockerImp;
    }

    @RequestMapping(value = {"/releaseByCCardId/{id}"}, method = RequestMethod.GET, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<List<String>> releaseByCCardId(@PathVariable("id") long id) {
        List<String> rowIds = cCardRepository.getById(id).getRowIDs();
        dataLockerImp.release(rowIds);
        return new ResponseEntity<>(rowIds, HttpStatus.OK);
    }

    @RequestMapping(value = {"/releaseAll"}, method = RequestMethod.GET, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<Integer> releaseAll() {
        dataLockerImp.releaseAll();
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = {"/{source}/getAvailable"}, method = RequestMethod.GET, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<CCard> getAvailable(@PathVariable("source") String source) {
        CCard cCard = cCardRepository.getAll(source).
                stream().
                filter(notLocked()).
                findAny().
                orElseThrow(() -> new ExistEntityException("Entity not found"));
        dataLockerImp.lock(cCard.getRowIDs());
        return new ResponseEntity<>(cCard, HttpStatus.OK);
    }

    @RequestMapping(value = "/{source}/getAvailable", method = RequestMethod.POST, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<CCard> getAvailableByParams(@PathVariable("source") String source, @RequestBody String filter) {
        CCard cCard = cCardRepository.getAll(source)
                .stream()
                .filter(checkHasParams(filter))
                .filter(notLocked())
                .findAny()
                .orElseThrow(() -> new ExistEntityException("Entity not found"));
        dataLockerImp.lock(cCard.getRowIDs());
        return new ResponseEntity<>(cCard, HttpStatus.OK);
    }
}
