package ru.bellintegrator.dataProvider.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.bellintegrator.dataProvider.exception.ExistEntityException;
import ru.bellintegrator.dataProvider.model.ClientPersonCard;
import ru.bellintegrator.dataProvider.repository.ClientPersonCardRepository;
import ru.bellintegrator.dataProvider.utils.dataLocker.DataLockerImp;

import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static ru.bellintegrator.dataProvider.utils.Predicates.checkHasParams;
import static ru.bellintegrator.dataProvider.utils.Predicates.notLocked;

@RestController
@RequestMapping(value = {"/ClientsPersonCards"})
public class ClientPersonCardController {

    private final ClientPersonCardRepository clientRepository;
    private final DataLockerImp dataLockerImp;

    @Autowired
    public ClientPersonCardController(ClientPersonCardRepository clientRepository, DataLockerImp dataLockerImp) {
        this.clientRepository = clientRepository;
        this.dataLockerImp = dataLockerImp;
    }


    @RequestMapping(value = {"/releaseByClientId/{id}"}, method = RequestMethod.GET, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<List<String>> releaseByClientId(@PathVariable("id") long id) {
        List<String> rowIds = clientRepository.getById(id).getRowIDs();
        dataLockerImp.release(rowIds);
        return new ResponseEntity<>(rowIds, HttpStatus.OK);
    }

    @RequestMapping(value = {"/releaseAll"}, method = RequestMethod.GET, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<Integer> releaseAll() {
        dataLockerImp.releaseAll();
        return new ResponseEntity<>(HttpStatus.OK);
    }


    @RequestMapping(value = {"/{source}/getAvailable"}, method = RequestMethod.GET, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<ClientPersonCard> getAvailable(@PathVariable("source") String source) {
        ClientPersonCard clientPersonCard = clientRepository.getAll(source).
                stream().
                filter(notLocked()).
                findAny().
                orElseThrow(() -> new ExistEntityException("Entity not found"));
        dataLockerImp.lock(clientPersonCard.getRowIDs());
        return new ResponseEntity<>(clientPersonCard, HttpStatus.OK);
    }

    @RequestMapping(value = "/{source}/getAvailable", method = RequestMethod.POST, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<ClientPersonCard> getAvailableByParams(@PathVariable("source") String source, @RequestBody String filter) {
        ClientPersonCard clientPersonCard = clientRepository.getAll(source)
                .stream()
                .filter(checkHasParams(filter))
                .filter(notLocked())
                .findAny()
                .orElseThrow(() -> new ExistEntityException("Entity not found"));
        dataLockerImp.lock(clientPersonCard.getRowIDs());
        return new ResponseEntity<>(clientPersonCard, HttpStatus.OK);
    }

}
