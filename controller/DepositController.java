package ru.bellintegrator.dataProvider.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.bellintegrator.dataProvider.exception.ExistEntityException;
import ru.bellintegrator.dataProvider.model.Deposit;
import ru.bellintegrator.dataProvider.repository.DepositRepository;
import ru.bellintegrator.dataProvider.utils.dataLocker.DataLockerImp;

import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static ru.bellintegrator.dataProvider.utils.Predicates.checkHasParams;
import static ru.bellintegrator.dataProvider.utils.Predicates.notLocked;

@RestController
@RequestMapping(value = {"/depos"})
public class DepositController {
    private final DepositRepository depositRepository;
    private final DataLockerImp dataLockerImp;

    @Autowired
    public DepositController(DepositRepository depositRepository, DataLockerImp dataLockerImp) {
        this.depositRepository = depositRepository;
        this.dataLockerImp = dataLockerImp;
    }

    @RequestMapping(value = {"/releaseByDepositId/{id}"}, method = RequestMethod.GET, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<List<String>> releaseByDepositId(@PathVariable("id") long id) {
        List<String> rowIds = depositRepository.getById(id).getRowIDs();
        dataLockerImp.release(rowIds);
        return new ResponseEntity<>(rowIds, HttpStatus.OK);
    }

    @RequestMapping(value = {"/releaseAll"}, method = RequestMethod.GET, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<Integer> releaseAll() {
        dataLockerImp.releaseAll();
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = {"/{source}/getAvailable"}, method = RequestMethod.GET, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<Deposit> getAvailable(@PathVariable("source") String source) {
        Deposit deposit = depositRepository.getAll(source).
                stream().
                filter(notLocked()).
                findAny().
                orElseThrow(() -> new ExistEntityException("Entity not found"));
        dataLockerImp.lock(deposit.getRowIDs());
        return new ResponseEntity<>(deposit, HttpStatus.OK);
    }

    @RequestMapping(value = "/{source}/getAvailable", method = RequestMethod.POST, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<Deposit> getAvailableByParams(@PathVariable("source") String source, @RequestBody String filter) {
        Deposit deposit = depositRepository.getAll(source)
                .stream()
                .filter(checkHasParams(filter))
                .filter(notLocked())
                .findAny()
                .orElseThrow(() -> new ExistEntityException("Entity not found"));
        dataLockerImp.lock(deposit.getRowIDs());
        return new ResponseEntity<>(deposit, HttpStatus.OK);
    }
}
