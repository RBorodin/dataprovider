package ru.bellintegrator.dataProvider.repository;

import com.fasterxml.jackson.core.type.TypeReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import ru.bellintegrator.dataProvider.exception.ExistEntityException;
import ru.bellintegrator.dataProvider.model.Deposit;

import java.util.List;

import static ru.bellintegrator.dataProvider.utils.dataFormatter.CsvUtils.readCsvToBeanList;
import static ru.bellintegrator.dataProvider.utils.dataFormatter.DBUtils.getNamedParameterJdbcTemplate;
import static ru.bellintegrator.dataProvider.utils.dataFormatter.DBUtils.readSqlCommandFromFile;
import static ru.bellintegrator.dataProvider.utils.dataFormatter.JsonUtils.fromJSON;

public class DepositRepository implements GenericRepository<Deposit> {
    private static final Logger LOGGER = LoggerFactory.getLogger(DepositRepository.class);
    private final BeanPropertyRowMapper<Deposit> beanPropertyRowMapper;

    public DepositRepository() {
        beanPropertyRowMapper = new BeanPropertyRowMapper<>(Deposit.class);
    }

    @Override
    public List<Deposit> getAll(String source) {
        List<Deposit> depositList = null;
        switch (source) {
            case ("db"):
                depositList = getNamedParameterJdbcTemplate().query(readSqlCommandFromFile("selectDepositDetails.sql"), beanPropertyRowMapper);
                break;
            case ("json"):
                depositList = fromJSON(new TypeReference<>() {
                }, "deposits.json");
                break;
            case ("csv"):
                try {
                    depositList = readCsvToBeanList("deposits.csv", Deposit.class);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            default:
                throw new ExistEntityException("неизвестный параметр источника данных");
        }
        return depositList;
    }

    @Override
    public Deposit getById(long id) {
        SqlParameterSource param = new MapSqlParameterSource("id", id);
        LOGGER.info("получен параметр поиска: {}", param);
        return getNamedParameterJdbcTemplate().query(readSqlCommandFromFile("selectDepositDetailsByID.sql"), param, beanPropertyRowMapper)
                .stream()
                .findFirst()
                .orElseThrow(() -> new ExistEntityException("Entity not found"));
    }
}
