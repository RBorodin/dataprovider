package ru.bellintegrator.dataProvider.repository;

import com.fasterxml.jackson.core.type.TypeReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import ru.bellintegrator.dataProvider.exception.ExistEntityException;
import ru.bellintegrator.dataProvider.model.CCard;

import java.util.List;

import static ru.bellintegrator.dataProvider.utils.dataFormatter.CsvUtils.readCsvToBeanList;
import static ru.bellintegrator.dataProvider.utils.dataFormatter.DBUtils.getNamedParameterJdbcTemplate;
import static ru.bellintegrator.dataProvider.utils.dataFormatter.DBUtils.readSqlCommandFromFile;
import static ru.bellintegrator.dataProvider.utils.dataFormatter.JsonUtils.fromJSON;

public class CCardRepository implements GenericRepository<CCard> {
    private static final Logger LOGGER = LoggerFactory.getLogger(CCardRepository.class);
    private final BeanPropertyRowMapper<CCard> beanPropertyRowMapper;

    public CCardRepository() {
        beanPropertyRowMapper = new BeanPropertyRowMapper<>(CCard.class);
    }

    @Override
    public List<CCard> getAll(String source) {
        List<CCard> cCardList = null;
        switch (source) {
            case ("db"):
                cCardList = getNamedParameterJdbcTemplate().query(readSqlCommandFromFile("selectCCardDetails.sql"), beanPropertyRowMapper);
                break;
            case ("json"):
                cCardList = fromJSON(new TypeReference<>() {
                }, "cCards.json");
                break;
            case ("csv"):
                try {
                    cCardList = readCsvToBeanList("cCards.csv", CCard.class);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            default:
                throw new ExistEntityException("неизвестный параметр источника данных");
        }
        return cCardList;
    }

    @Override
    public CCard getById(long id) {
        SqlParameterSource param = new MapSqlParameterSource("id", id);
        LOGGER.info("получен параметр поиска: {}", param);
        return getNamedParameterJdbcTemplate().query(readSqlCommandFromFile("selectCCardDetailsByID.sql"), param, beanPropertyRowMapper)
                .stream()
                .findFirst()
                .orElseThrow(() -> new ExistEntityException("Entity not found"));
    }
}
