package ru.bellintegrator.dataProvider.repository;

import com.fasterxml.jackson.core.type.TypeReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Component;
import ru.bellintegrator.dataProvider.exception.ExistEntityException;
import ru.bellintegrator.dataProvider.model.ClientPersonCard;

import java.util.List;

import static ru.bellintegrator.dataProvider.utils.dataFormatter.CsvUtils.readCsvToBeanList;
import static ru.bellintegrator.dataProvider.utils.dataFormatter.DBUtils.getNamedParameterJdbcTemplate;
import static ru.bellintegrator.dataProvider.utils.dataFormatter.DBUtils.readSqlCommandFromFile;
import static ru.bellintegrator.dataProvider.utils.dataFormatter.JsonUtils.fromJSON;

@Component
public class ClientPersonCardRepository implements GenericRepository<ClientPersonCard> {
    private static final Logger LOGGER = LoggerFactory.getLogger(ClientPersonCardRepository.class);
    private final BeanPropertyRowMapper<ClientPersonCard> beanPropertyRowMapper;

    public ClientPersonCardRepository() {
        beanPropertyRowMapper = new BeanPropertyRowMapper<>(ClientPersonCard.class);
    }

    @Override
    public List<ClientPersonCard> getAll(String source) {
        List<ClientPersonCard> clientPersonCardList = null;
        switch (source) {
            case ("db"):
                clientPersonCardList = getNamedParameterJdbcTemplate().query(readSqlCommandFromFile("selectClientPersonCardDetails.sql"), beanPropertyRowMapper);
                break;
            case ("json"):
                clientPersonCardList = fromJSON(new TypeReference<>() {
                }, "clientPersonCards.json");
                break;
            case ("csv"):
                try {
                    clientPersonCardList = readCsvToBeanList("clientPersonCards.csv", ClientPersonCard.class);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            default:
                throw new ExistEntityException("неизвестный параметр источника данных");
        }
        return clientPersonCardList;
    }

    @Override
    public ClientPersonCard getById(long id) {
        SqlParameterSource param = new MapSqlParameterSource("id", id);
        LOGGER.info("получен параметр поиска: {}", param);
        return getNamedParameterJdbcTemplate().query(readSqlCommandFromFile("selectClientPersonCardDetailsbyID.sql"), param, beanPropertyRowMapper)
                .stream()
                .findFirst()
                .orElseThrow(() -> new ExistEntityException("Entity not found"));
    }
}
