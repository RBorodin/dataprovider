package ru.bellintegrator.dataProvider.repository;

import com.fasterxml.jackson.core.type.TypeReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Component;
import ru.bellintegrator.dataProvider.exception.ExistEntityException;
import ru.bellintegrator.dataProvider.model.Branch;

import java.util.List;

import static ru.bellintegrator.dataProvider.utils.dataFormatter.CsvUtils.readCsvToBeanList;
import static ru.bellintegrator.dataProvider.utils.dataFormatter.DBUtils.getNamedParameterJdbcTemplate;
import static ru.bellintegrator.dataProvider.utils.dataFormatter.DBUtils.readSqlCommandFromFile;
import static ru.bellintegrator.dataProvider.utils.dataFormatter.JsonUtils.fromJSON;

@Component
public class BranchRepository implements GenericRepository<Branch> {
    private static final Logger LOGGER = LoggerFactory.getLogger(BranchRepository.class);
    private final BeanPropertyRowMapper<Branch> beanPropertyRowMapper;

    public BranchRepository() {
        beanPropertyRowMapper = new BeanPropertyRowMapper<>(Branch.class);
    }

    @Override
    public List<Branch> getAll(String source) {
        List<Branch> branchList = null;
        switch (source) {
            case ("db"):
                branchList = getNamedParameterJdbcTemplate().query(readSqlCommandFromFile("selectBranchDetails.sql"), beanPropertyRowMapper);
                break;
            case ("json"):
                branchList = fromJSON(new TypeReference<>() {
                }, "Branches.json");
                break;
            case ("csv"):
                try {
                    branchList = readCsvToBeanList("Branches.csv", Branch.class);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            default:
                throw new ExistEntityException("неизвестный параметр источника данных");
        }
        return branchList;
    }

    @Override
    public Branch getById(long id) {
        SqlParameterSource param = new MapSqlParameterSource("id", id);
        LOGGER.info("получен параметр поиска: {}", param);
        return getNamedParameterJdbcTemplate().query(readSqlCommandFromFile("selectBranchDetailsByID.sql"), param, beanPropertyRowMapper)
                .stream()
                .findFirst()
                .orElseThrow(() -> new ExistEntityException("Entity not found"));
    }
}
