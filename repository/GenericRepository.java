package ru.bellintegrator.dataProvider.repository;

import java.util.List;

public interface GenericRepository<T> {

    List<T> getAll(String source);

    T getById(long id);
}
