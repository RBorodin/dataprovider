package ru.bellintegrator.dataProvider;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class TestDataProviderApplication {


    public static void main(String[] args) {
        SpringApplication.run(TestDataProviderApplication.class, args);
    }


}
