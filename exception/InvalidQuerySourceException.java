package ru.bellintegrator.dataProvider.exception;

public class InvalidQuerySourceException extends RuntimeException {

    public InvalidQuerySourceException(String msg) {
        super(msg);
    }
}
