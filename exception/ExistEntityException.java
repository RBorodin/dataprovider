package ru.bellintegrator.dataProvider.exception;


public class ExistEntityException extends RuntimeException {

    public ExistEntityException(String msg) {
        super(msg);
    }
}